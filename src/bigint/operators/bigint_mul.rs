use crate::bigint::BigInt;
use crate::bigint::utilities::*;

use std::io::*;
use std::ops::*;
use std::cmp::*;
use std::convert::TryInto;

fn doubleadd_mul(a: &BigInt, b: &BigInt) -> BigInt {
    /*
     * 
    def mult(a, b):
        add_buffer = 0
        top = a
        while b > 1:
            digit = b % 2
            if digit == 1:
                add_buffer = add_buffer + top

            top = top << 1
            b = b >> 1
        return top + add_buffer 
     */
    
    let zero:BigInt  = create_bigint_from_string("0x0").unwrap();
    let one:BigInt  = create_bigint_from_string("0x1").unwrap();

    if a.compare(&zero) == 0 || b.compare(&zero) == 0 || a.data.len() == 0 || b.data.len() == 0 {
        // Hardcode multiply by zero
        std::io::stdout().flush().unwrap();
        return zero.clone();
    }

    let mut b_copy:BigInt = b.clone();
    b_copy.negative = false; // set to false temporarily and calculate sign at the end

    let mut add_buffer:BigInt = zero.clone();
    let mut top:BigInt = a.clone();

    while b_copy > one.clone() {
        let digit:u64 = b_copy.data[0] % 2;

        if digit == 1 {
            add_buffer = add_buffer + top.clone();
        }
        top = top.clone() << one.clone();
        b_copy = b_copy >> one.clone();
    }
    
    let mut result:BigInt = top + add_buffer;
    // calculate the correct sign value
    if a.negative != b.negative {
        result.negative = true;
    } else {
        result.negative = false;
    }

    return result;

}

impl Mul for BigInt {
    type Output = BigInt;

    fn mul(self, b:BigInt) -> BigInt {
        return &self * &b;
    }
}

impl<'a> Mul<&'a BigInt> for BigInt {
    type Output = BigInt;

    fn mul(self, b:&'a BigInt) -> BigInt {
        return &self * b;
    }
} 
impl<'a> Mul<BigInt> for &'a BigInt {
    type Output = BigInt;

    fn mul(self, b: BigInt) -> BigInt {
        return self * &b;
    }
}

impl<'a,'b> Mul<&'a BigInt> for &'b BigInt {
    type Output = BigInt;

    fn mul(self, b: &'a BigInt) -> BigInt {
        let mut interim:BigInt = doubleadd_mul(self, b);

        if interim.data.len() == 1 && interim.data[0] == 0 {
            interim.negative = false;
        }

        return interim;
    }
}
