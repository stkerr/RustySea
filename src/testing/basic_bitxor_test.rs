extern crate rusty_sea;
use rusty_sea::bigint::*;
use rusty_sea::bigint::utilities::*;

#[test]
fn basic_bitxor_test() {
    let a:BigInt = create_bigint_from_string("0x0F0F").unwrap();
    let b:BigInt = create_bigint_from_string("0xF0F0").unwrap();
    let c:BigInt = create_bigint_from_string("0xFFFF").unwrap();
    assert!((a^b).compare(&c) == 0);
}

#[test]
fn basic_bitxor_test_length2_interblockdifference() {
    let a:BigInt = create_bigint_from_string("0x0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F").unwrap();
    let b:BigInt = create_bigint_from_string("0xF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0").unwrap();
    let c:BigInt = create_bigint_from_string("0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF").unwrap();
    assert!((a^b).compare(&c) == 0);
}

#[test]
fn basic_bitxor_test_length2_fulldifference() {
    let a:BigInt = create_bigint_from_string("0xFFFFFFFFFFFFFFFF0000000000000000").unwrap();
    let b:BigInt = create_bigint_from_string("0x0000000000000000FFFFFFFFFFFFFFFF").unwrap();
    let c:BigInt = create_bigint_from_string("0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF").unwrap();
    assert!((a^b).compare(&c) == 0);
}

#[test]
fn basic_bitxor_test_unequal_length() {
    let a:BigInt = create_bigint_from_string("0xFFFFFFFFFFFFFFFF").unwrap();
    let b:BigInt = create_bigint_from_string("0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF").unwrap();
    let c:BigInt = create_bigint_from_string("0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000").unwrap();
    assert!((a^b).compare(&c) == 0);
}

op_test!(basic_bitxor_1, "0xFFFFFFFFFFFFFFFF" ^ "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF" == "0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000");

op_test!(basic_bitxor_2, "-0x1" ^ "0x3" == "-0x4");
op_test!(basic_bitxor_3, "0x3" ^ "-0x1" == "-0x4");
op_test!(basic_bitxor_4, "-0x3" ^ "-0x1" == "0x2");
op_test!(basic_bitxor_5, "-0x1" ^ "-0x3" == "0x2");
op_test!(basic_bitxor_6, "-0x2" ^ "0x3" == "-0x3");
op_test!(basic_bitxor_7, "0x2" ^ "-0x3" == "-0x1");
op_test!(basic_bitxor_8, "0x10000000000000000" ^ "-0x1" == "-0x10000000000000001");
op_test!(basic_bitxor_9, "-0x1" ^ "0x10000000000000000" == "-0x10000000000000001");
op_test!(basic_bitxor_10, "0x10000000000000000" ^ "0x10000000000000000" == "0x0");
op_test!(basic_bitxor_11, "0x10000000000000000" ^ "-0x10000000000000000" == "-0x20000000000000000");
op_test!(basic_bitxor_12, "-0x10000000000000000" ^ "0x10000000000000000" == "-0x20000000000000000");